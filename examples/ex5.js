var builder = require('botbuilder');
var restify = require('restify');
var bot = require('../');
var recognizer = new builder.LuisRecognizer('https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/cf9b318a-5328-4fe3-b3f5-0fca3fd34384?subscription-key=01b63cc6fdd1461cab71d9ad79671b65');
var intents = new builder.IntentDialog({ recognizers: [recognizer] });

bot.dialog('/', intents);

intents.matches('greeting', '/greeting');
intents.matches('help', '/help');
intents.matches('travel', '/travel');

intents.matches('change name', [
    function (session) {
        session.beginDialog('/profile');
    },
    function (session, results) {
        session.send('Ok... Je naam is veranderd naar %s', session.userData.name);
    }
]);

intents.onDefault([
    function (session) {
        session.send("Sorry. Dat begrijp ik niet");
    },
]);

bot.dialog('/greeting', [
    function (session) {
        session.send("Ik ben Choo Choo, je trein assistent! Waarmee ik je vandaag helpen?");
        session.endDialog();
    }
]);

bot.dialog('/help', [
    function (session) {
        session.send("Ik kan je helpen bij het boeken van tickets of je naam veranderen");
        session.endDialog();
    }
]);


bot.dialog('/travel', [
    function (session, args, next) {
        session.userData.ticket = {};
        var arrival = builder.EntityRecognizer.findEntity(args.entities, 'arrival');
        if (arrival) {
          session.userData.ticket.arrival = arrival.entity;
        }
        if (!session.userData.name) {
            session.beginDialog('/profile');
        } else {
            next();
        }
    },
    function(session, results) {
      builder.Prompts.text('Van waar wil je vertrekken?');
    },
    function (session, results, next) {
        // Store the departure location
        session.userData.ticket.departure = results.response;
        if(!session.userData.ticket.arrival) {
          // Ask for the arrival location
          builder.Prompts.choice(session, 'Waar moet je zijn?', ['Brussel', 'Antwerpen', 'Gent']);
        } else {
          next();
        }

    },
    function (session, results) {
        // Store the arrival location
        if (results.response) {
          session.userData.ticket.arrival = results.response.entity;
        }
        // Display the results
        session.beginDialog('/cards');
    }
]);

bot.dialog('/cards', [
    function (session) {
        var msg = new builder.Message(session)
            .textFormat(builder.TextFormat.xml)
            .attachments([
                new builder.HeroCard(session)
                    .title("9u15 -> 10u11")
                    .subtitle("0 wijzigingen")
                    .text(session.userData.ticket.departure + " -> " + session.userData.ticket.arrival)
                    .tap(builder.CardAction.openUrl(session, "https://i.ytimg.com/vi/P07FchevFqE/hqdefault.jpg")),
                new builder.HeroCard(session)
                    .title("9u37 -> 10u59")
                    .subtitle("1 wijziging")
                    .text(session.userData.ticket.departure + " -> " + session.userData.ticket.arrival)
                    .tap(builder.CardAction.openUrl(session, "https://i.ytimg.com/vi/P07FchevFqE/hqdefault.jpg"))
            ]);
        session.endDialog(msg);
    }
]);

bot.dialog('/profile', [
    function (session) {
        builder.Prompts.text(session, 'Ok, Wat is jouw naam?');
    },
    function (session, results) {
        session.userData.name = results.response;
        session.endDialog();
    }
]);
