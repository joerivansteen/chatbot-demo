var builder = require('botbuilder');
var bot = require('../');

bot.dialog('/', [
    function (session, results, next) {
        if (!session.userData.name) {
            session.beginDialog('/profile');
        } else {
            next();
        }
    },
    function(session, results) {
        builder.Prompts.text(session, 'Dag ' + session.userData.name + '. Van waar wil je vertrekken?');
    },
    function (session, results) {
        // Store the departure location
        session.userData.departure = results.response;

        builder.Prompts.choice(session, 'Waar moet je zijn?', ['Brussel', 'Antwerpen', 'Gent']);
    },
    function (session, results) {
        // Store the arrival location
        session.userData.arrival = results.response.entity;

        builder.Prompts.time(session, 'Wanneer wil je vertrekken?');
    },
    function (session, results) {
        // Store the departure time
        session.userData.departureTime = builder.EntityRecognizer.resolveTime([results.response]);

        session.send('Bedankt om met ons te reizen van ' + session.userData.departure + ' tot ' + session.userData.arrival + '!');
    }
]);

bot.dialog('/profile', [
    function (session) {
        builder.Prompts.text(session, 'Hallo! Hoe heet jij?');
    },
    function (session, results) {
        session.userData.name = results.response;
        session.endDialog();
    }
]);
