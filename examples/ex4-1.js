var builder = require('botbuilder');
var bot = require('../');
var intents = new builder.IntentDialog();

bot.dialog('/', intents);

intents.matches(/^reizen/i, '/travel');

intents.matches(/^verander naam/i, [
    function (session) {
        session.beginDialog('/profile');
    },
    function (session, results) {
        session.send('Ok... Je naam is veranderd naar %s', session.userData.name);
    }
]);

intents.onDefault([
    function (session) {
        session.send("Sorry. Dat begrjp ik niet. Ik ken enkel 'reizen' of 'verander naam'.");
    },
]);


bot.dialog('/travel', [
    function (session, results, next) {
        if (!session.userData.name) {
            session.beginDialog('/profile');
        } else {
            next();
        }
    },
    function(session, results) {
        builder.Prompts.text(session, 'Dag ' + session.userData.name + '. Van waar wil je vertrekken?');
    },
    function (session, results) {
        // Store the departure location
        session.userData.departure = results.response;
        // Ask for the arrival location
        builder.Prompts.choice(session, 'Waar moet je zijn?', ['Brussel', 'Antwerpen', 'Gent']);

    },
    function (session, results) {
        // Store the arrival location
        session.userData.arrival = results.response.entity;
        // Display the results
        session.beginDialog('/cards');
    }
]);

bot.dialog('/cards', [
    function (session) {
        var msg = new builder.Message(session)
            .textFormat(builder.TextFormat.xml)
            .attachments([
                new builder.HeroCard(session)
                    .title("9u15 -> 10u11")
                    .subtitle("0 wijzigingen")
                    .text(session.userData.departure + " -> " + session.userData.arrival)
                    .tap(builder.CardAction.openUrl(session, "https://i.ytimg.com/vi/P07FchevFqE/hqdefault.jpg")),
                new builder.HeroCard(session)
                    .title("9u37 -> 10u59")
                    .subtitle("1 wijziging")
                    .text(session.userData.departure + " -> " + session.userData.arrival)
                    .tap(builder.CardAction.openUrl(session, "https://i.ytimg.com/vi/P07FchevFqE/hqdefault.jpg"))
            ]);
        session.endDialog(msg);
    }
]);

bot.dialog('/profile', [
    function (session) {
        builder.Prompts.text(session, 'Hallo! Wat is jouw naam?');
    },
    function (session, results) {
        session.userData.name = results.response;
        session.endDialog();
    }
]);
