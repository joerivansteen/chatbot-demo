var builder = require('botbuilder');
var bot = require('../');

bot.dialog('/', [
    function (session) {
        builder.Prompts.text(session, 'Hallo! Hoe heet jij?');
    },
    function (session, results) {
        session.send('Dag %s!', results.response);
    }
]);
