var builder = require('botbuilder');
var bot = require('../');
var recognizer = new builder.LuisRecognizer('https://api.projectoxford.ai/luis/v1/application?id=b6e2d4b5-3125-4e13-b364-41e0a2cdfecd&subscription-key=aa3c62de6d834399be51eca922b005ef');
var intents = new builder.IntentDialog({ recognizers: [recognizer] });

bot.dialog('/', intents);

intents.matches('travel', '/travel');

intents.matches('changeName', [
  function (session) {
      session.beginDialog('/profile');
  },
  function (session, results) {
      session.send('Ok... Je naam is veranderd naar %s', session.userData.name);
  }
]);

intents.onDefault([
    function (session) {
      session.send("Sorry. Dat begrjp ik niet. Ik ken enkel 'reizen' of 'verander naam'.");
    },
]);

bot.dialog('/travel', [
    function (session, results, next) {
        if (!session.userData.name) {
            session.beginDialog('/profile');
        } else {
            next();
        }
    },
    function(session, results) {
        builder.Prompts.text(session, 'Dag ' + session.userData.name + '. Van waar wil je vertrekken?');
    },
    function (session, results) {
        // Store the departure location
        session.userData.departure = results.response;
        // Ask for the arrival location
        builder.Prompts.choice(session, 'Waar moet je zijn?', ['Brussel', 'Antwerpen', 'Gent']);

    },
    function (session, results) {
        // Store the arrival location
        session.userData.arrival = results.response.entity;
        // Display the results
        session.beginDialog('/picture');
    }
]);

bot.dialog('/picture', [
    function (session) {
        session.send("Deze trein brengt je naar je bestemming");
        var msg = new builder.Message(session)
            .attachments([{
                contentType: "image/jpeg",
                contentUrl: "https://i.ytimg.com/vi/P07FchevFqE/hqdefault.jpg"
            }]);
        session.endDialog(msg);
    }
]);

bot.dialog('/profile', [
    function (session) {
        builder.Prompts.text(session, 'Hallo! Wat is jouw naam?');
    },
    function (session, results) {
        session.userData.name = results.response;
        session.endDialog();
    }
]);
