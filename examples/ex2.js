var builder = require('botbuilder');
var bot = require('../');

bot.dialog('/', [
    function (session) {
        builder.Prompts.text(session, 'Van waar wil je vertrekken?');
    },
    function (session, results) {
        // Store the departure location
        session.userData.departure = results.response;

        // Ask for the arrival location
        builder.Prompts.choice(session, 'Waar moet je zijn?', ['Brussel', 'Antwerpen', 'Gent']);

    },
    function (session, results) {
        // Store the arrival location
        session.userData.arrival = results.response.entity;

        // Display the results
        session.send('Bedankt om met ons te reizen van ' + session.userData.departure + ' tot ' + session.userData.arrival + '!');
    }
]);
